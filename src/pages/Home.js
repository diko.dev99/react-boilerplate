import React from 'react'
import { DefaultLayout } from '../layouts/default'

export const Home = () => {
  return (
    <DefaultLayout>
      Home Page
    </DefaultLayout>
  )
}
