import React from 'react'
import PropTypes from 'prop-types'
import { Layout } from 'antd'

export const DefaultLayout = ({ children }) => {
  return (
    <Layout>
      <Layout.Content>
        {/* add your components like header, sidebar, etc */}
      </Layout.Content>
      {children} {/* prefer to create a dashboard */}
    </Layout>
  )
}

DefaultLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.object
  ])
}
