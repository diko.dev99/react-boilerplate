/* eslint-disable no-undef */
import React from 'react'
import PropTypes from 'prop-types'
import {
  Route,
  Routes,
  Navigate,
  BrowserRouter
} from 'react-router-dom'

export const PrivateRouter = ({ children }) => {
  const auth = useAuth()
  return auth ? children : <Navigate to='/login' />
}

export const ReverseRouter = ({ children }) => {
  const auth = useAuth()
  return auth ? <Navigate to='/' /> : children
}

const useAuth = () => {
  const isAuthorized = localStorage.getItem('')
  const isAuthenticated = localStorage.getItem('')

  return isAuthorized && isAuthenticated
}

export const Routers = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path='/'
          element={
            <PrivateRouter>{/* your pages with protected router */}</PrivateRouter>
          }
        />
        <Route
          path='/example/:id' // router path for id params
          element={
            <PrivateRouter>{/* your pages with protected router */}</PrivateRouter>
          }
        />
        <Route
          path='/'
          element={
            <ReverseRouter>{/* remove access to previous page  */}</ReverseRouter>
          }
        />
        <Route path='*' element={[]} /> {/* route for handler page not found 404  */}
      </Routes>
    </BrowserRouter>
  )
}

PrivateRouter.propTypes = {
  children: PropTypes.object
}

ReverseRouter.propTypes = {
  children: PropTypes.object
}
