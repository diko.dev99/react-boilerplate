import helper from './helper'
/*
  @(token, params)
  use this params for token, or params { headers }
  make sure, you really want to use, otherwise is no need to params.
  option
*/

export const example = {
  get: (token, params) => helper.get('https://examples.com/v1', token, params),
  post: (token, params) => helper.post('https://examples.com/v1', token, params),
  patch: (token, params) => helper.patch('https://examples.com/v1/params/id', token, params),
  delete: (token, params) => helper.delete('https://examples.com/v1/params/id', token, params)

  // if you handle delete is having problems, try this one
  /*
    delete: (token, params) => axios.delete(''https://examples/com/id,
     { headers: { Authorization: `Bearer ${token}` }})
     .catch(err => err.response)
  */
}
