import { actions } from './actions'

const initialState = {
  loading: false // you can add more than one state, what you need
}

export const exampleReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.set_state:
      return { ...state, ...action.payload } // do something
    default:
      return state
  }
}
