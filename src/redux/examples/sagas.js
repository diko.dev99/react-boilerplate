import { all, put, takeEvery } from 'redux-saga/effects'

import { actions } from 'src/redux/examples/actions'

export function * setExample ({ payload }) {
  yield put({
    type: 'example/set_state',
    payload: {
      loading: true // just example for do something
    }
  })
}

export default function * rootSaga () {
  yield all([
    takeEvery(actions.set_state, setExample) // export all function on yield all
  ])
}
