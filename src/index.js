import React from 'react'
import ReactDOM from 'react-dom'

import 'src/assets/styles/global.scss'
import 'antd/dist/antd.css'

import reducers from 'src/redux/reducers'
import sagas from 'src/redux/sagas'
import { Routers } from 'src/routers'

import logger from 'redux-logger'
import createSagaMiddleware from '@redux-saga/core'
import { Provider } from 'react-redux'
import { applyMiddleware, compose, createStore } from 'redux'

const sagaMiddleware = createSagaMiddleware()
const middlewares = [sagaMiddleware]

if (process.env.NODE_ENV === 'development' && true) {
  middlewares.push(logger)
}

const store = createStore(reducers(), compose(applyMiddleware(...middlewares)))
sagaMiddleware.run(sagas)

require('dotenv').config()

ReactDOM.render(
  <Provider store={store}>
    <Routers />
  </Provider>,
  document.getElementById('root')
)

export { store }
